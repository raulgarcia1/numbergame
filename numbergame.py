import random


def main():
    print "Wanna play a game?"
    print "Guess a number between 1 and 100"
    randomNumber = random.randint(1,100)
    found = False        #flag variable too see
                         # if they guessed it
    while not found:
        userGuess = input("Your Guess: ")
        if userGuess == randomNumber:
            print "You got it!"
            found = True
        elif userGuess > randomNumber:
            print "Guess lower"
        else:
            print "Guess higher"

    print "Wow you won :) thanks for playing for now ;)"


if __name__ == "__main__":
    main()